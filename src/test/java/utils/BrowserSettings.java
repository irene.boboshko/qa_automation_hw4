package utils;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

public abstract class BrowserSettings {
    protected EventFiringWebDriver driver;

    @Parameters("browser")
    @BeforeTest
    public void setUpClass(@Optional("chrome") String browser) {
        Reporter.log(browser.toUpperCase() + " has started");
        if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty(
                    "webdriver.gecko.driver",
                    new File(BrowserSettings.class.getResource("/drivers/geckodriver.exe").getFile()).getPath());
            driver = new EventFiringWebDriver(new FirefoxDriver());

        } else if (browser.equalsIgnoreCase("ie") || browser.equals("internet explorer")) {
            System.setProperty(
                    "webdriver.ie.driver",
                    new File(BrowserSettings.class.getResource("/drivers/IEDriverServer.exe").getFile()).getPath());
            driver = new EventFiringWebDriver(new InternetExplorerDriver());

        } else if (browser.equalsIgnoreCase("edge")) {
            System.setProperty(
                    "webdriver.edge.driver",
                    new File(BrowserSettings.class.getResource("/drivers/MicrosoftWebDriver.exe").getFile()).getPath());
            driver = new EventFiringWebDriver(new EdgeDriver());

        } else if (browser.equalsIgnoreCase("opera")) {
            System.setProperty(
                    "webdriver.opera.driver",
                    new File(BrowserSettings.class.getResource("/drivers/operadriver.exe").getFile()).getPath());
            driver = new EventFiringWebDriver(new OperaDriver());

        } else {
            System.setProperty(
                    "webdriver.chrome.driver",
                    new File(BrowserSettings.class.getResource("/drivers/chromedriver.exe").getFile()).getPath());
            driver = new EventFiringWebDriver(new ChromeDriver());
        }
    }

    @BeforeClass
    public void setUp() {
        //maximize the window, if the Firefox driver is NOT used
        if (!driver.getWrappedDriver().toString().contains("FirefoxDriver")) {
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/index.php?controller=AdminLogin&token=d251f363cceb5a849cb7330938c64dea");
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}