package preconditions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings({"ALL", "CanBeFinal"})
public class GoodsDetailsPage {
    private WebDriver driver;

    public GoodsDetailsPage(WebDriver driver) {
        this.driver = driver;
    }

    //Locators
    private String itemNameLocator = "//h1[@itemprop]";
    private String priceLocator = "//span[@itemprop='price']";
    private String quantityLocator = "//span[contains(text(), 'Товары')]";

    public String getItemNameDisplayed() {
        return driver.findElement(By.xpath(itemNameLocator)).getText();
    }

    public String getPriceDisplayed() {
        return driver.findElement(By.xpath(priceLocator)).getText();
    }

    public int getQuantityDisplayed() {
        String quantity = driver.findElement(By.xpath(quantityLocator)).getText();
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(quantity);
        while (matcher.find()) {
            quantity = matcher.group();
        }
        return Integer.parseInt(quantity);
   }
}