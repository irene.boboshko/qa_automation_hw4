package preconditions;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.UUID;

@SuppressWarnings({"ALL", "CanBeFinal"})
public class ItemGenerator {
    private static String item;
    private static int quantity;
    private static String price;

    private Random random;

    public ItemGenerator() {
        random = new Random();
    }

    public void generateItem() {
        UUID id = UUID.randomUUID();
        String word = id.toString().replaceAll("-", "");
        item = word.substring(0, 8);
    }

    public void generateQuantity() {
        quantity = random.ints(1, (100))
                .findFirst()
                .orElse(1);
    }

    public void generatePrice() {
        double randomPrice = random.doubles(0.1, (101 + 1)).findFirst().orElse(0.1);
        price = new DecimalFormat("#0.00").format(randomPrice);
    }

    public static String getItem() {
        return item;
    }

    public static int getQuantity() {
        return quantity;
    }

    public static String getPrice() {
        return price;
    }
}
