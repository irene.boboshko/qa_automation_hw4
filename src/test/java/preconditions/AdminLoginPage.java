package preconditions;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

@SuppressWarnings({"ALL", "CanBeFinal"})
@Getter
public class AdminLoginPage {
    private WebDriver driver;

    //Locators
    private String emailLocator = "email";
    private String passwordLocator = "passwd";
    private String enterButtonLocator = "submitLogin";

    public AdminLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private void enterEmail(String email) {
        driver.findElement(By.id(emailLocator)).sendKeys(email);
    }

    private void enterPassword(String password) {
        driver.findElement(By.id(passwordLocator)).sendKeys(password);
    }

    private void submitEnterButton() {
        driver.findElement(By.name(enterButtonLocator)).submit();
    }

    public void login(String email, String password) {
        Reporter.log("Logging in as Admin");
        enterEmail(email);
        enterPassword(password);
        submitEnterButton();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleContains("Dashboard"));
        new AdminDashboardPage(driver);
    }
}