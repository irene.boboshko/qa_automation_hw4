package preconditions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.util.List;

@SuppressWarnings({"ALL", "CanBeFinal"})
public class MainPage {
    private WebDriver driver;
    private List<WebElement> elements;
    private WebDriverWait wait;

    private String allGoodsLocator = "//div[@class='products']/following-sibling::a";
    private By lastItemsPage = By.xpath("//div[@class='col-md-6']//li[last()-1]/a");
    private By itemsList = By.xpath("//div[@class='product-description']/h1/a");

    public MainPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public void clickAllGoodsLink() {
        Reporter.log("Clicking the \"All Goods\" button");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(allGoodsLocator)));
        driver.findElement(By.xpath(allGoodsLocator)).click();
    }

    public boolean isItemDisplayed() {
        wait.until(ExpectedConditions.presenceOfElementLocated(lastItemsPage));
        driver.findElement(lastItemsPage).click();
        wait.until(ExpectedConditions.urlContains("http://prestashop-automation.qatestlab.com.ua/ru/2-home?page="));
        elements = driver.findElements(itemsList);
        openItemPage();
        return true;
    }

    private void openItemPage() {
        elements.get(elements.size() - 1).click();
        Reporter.log("The last item has been clicked");
        new GoodsDetailsPage(driver);
    }
}
